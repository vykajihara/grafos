import java.util.LinkedList;
import java.util.Comparator;

public class Vertice 
{
	enum cores {
		branco,
		cinza,
		preto	
	}	

	public int numero;
	public LinkedList<Vertice> listaAdjacentes;
	public int distancia; 
	public cores cor;
	public boolean visitado;
	public int pai;
	public float chave;
	public boolean estaNaLista;

    public Vertice(int n) 
	{
		this.numero = n;
		this.listaAdjacentes = new LinkedList<Vertice>();
		this.distancia = -1;
		this.cor = cores.branco;
		this.estaNaLista = false;
    }

	public void addVerticeListaAdjacentes(Vertice v)
	{
		this.listaAdjacentes.add(v);
	}
	
}

class CompVertice implements Comparator<Vertice> 
{
	@Override
    public int compare(Vertice v1, Vertice v2) 
	{
		if (v1.chave < v2.chave) {
			return -1;
		} else if (v1.chave > v2.chave) {
			return 1;
		} else {
			return 0;
		}	
    }
}

