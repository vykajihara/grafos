import java.util.Collections; 
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class RandomTreePrim {
		
    public static double diametroMedio(int n)
	{
		int soma = 0;
	
		for (int i = 1; i <= 3; i++) {
			Grafo g = Prim(n);

			if (g.eh_arvore())
				soma += DiametroGrafo.calcularDiametro(g);
			else
				System.out.println("Erro ao gerar arvore aleatoria");		
		}

		return soma/3.000;
	}
    

	public static Vertice extractMin(List<Vertice> list) 
	{
		Vertice v = Collections.min(list, new CompVertice());
		list.remove(v);
		v.estaNaLista = false;
		return v;
	} 

	public static Grafo mstPrim(Grafo g, int root) 
	{
		Grafo r = new Grafo(g.vertices.length);

		for (Vertice v : g.vertices) {
			v.chave = Float.MAX_VALUE;
			v.pai = -1;
			v.estaNaLista = true;
		}
		g.vertices[root].chave = 0;

		List<Vertice> vertices = new LinkedList<Vertice>();
		for (int i = 0; i < g.vertices.length; i++)
			vertices.add(g.vertices[i]);
		
		while (vertices.size() > 0) {
			Vertice u = extractMin(vertices);
			if (u != g.vertices[root]) 
				r.adicionarAresta(u.numero, u.pai, u.chave);
			
			for (Vertice v : g.vertices[u.numero].listaAdjacentes) {
				if (v.estaNaLista == true && g.pesos[u.numero][v.numero] < v.chave) {
					g.vertices[v.numero].pai = u.numero;
					g.vertices[v.numero].chave = g.pesos[u.numero][v.numero];
				}
			}
		}

		return r;
	}

	public static Grafo Prim(int n) 
	{
		Grafo g = new Grafo(n);
		Random r = new Random();

		float min = 0F;
		float max = 1F;
		
   		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				g.adicionarAresta(i, j, min + r.nextFloat() * (max - min));
			}
		}

		return mstPrim(g, r.nextInt(g.vertices.length));
	}

	public static void main(String[] args) 
	{
		/*Grafo g = new Grafo(5);
		g.adicionarAresta(0, 1, 0.7F);
		g.adicionarAresta(0, 2, 0.3F);
		g.adicionarAresta(0, 3, 0.35F);
		g.adicionarAresta(0, 4, 0.8F);
		g.adicionarAresta(1, 2, 0.15F);
		g.adicionarAresta(1, 3, 0.58F);
		g.adicionarAresta(1, 4, 0.2F);
		g.adicionarAresta(2, 3, 0.1F);
		g.adicionarAresta(2, 4, 0.5F);
		g.adicionarAresta(3, 4, 0.65F);
		
		assert(mstPrim(g, 0).eh_arvore() == true);
		assert(g.vertices[1].pai == 2);
		assert(g.vertices[1].chave == 0.15F);
		assert(g.vertices[2].pai == 0);
		assert(g.vertices[2].chave == 0.3F);
		assert(g.vertices[3].pai == 2);
		assert(g.vertices[3].chave == 0.1F);
		assert(g.vertices[4].pai == 1);
		assert(g.vertices[4].chave == 0.2F);
	
		Grafo g1 = new Grafo(9);
		g1.adicionarAresta(0, 1, 4F);
		g1.adicionarAresta(0, 7, 8F);
		g1.adicionarAresta(1, 2, 8F);
		g1.adicionarAresta(1, 7, 11F);
		g1.adicionarAresta(2, 3, 7F);
		g1.adicionarAresta(2, 5, 4F);
		g1.adicionarAresta(2, 8, 2F);
		g1.adicionarAresta(3, 4, 9F);
		g1.adicionarAresta(3, 5, 14F);
		g1.adicionarAresta(4, 5, 10F);
		g1.adicionarAresta(5, 6, 2F);
		g1.adicionarAresta(6, 7, 1F);

		Grafo g2= mstPrim(g1, 0);
		assert(g2.eh_arvore() == true); */

		for (int i = 250; i <= 2000; i += 250)
			System.out.println(diametroMedio(2000));

	}
}
