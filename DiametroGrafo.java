public class DiametroGrafo 
{
	public static int calcularDiametro(Grafo g)
	{
		Bfs.bfs(g, 0);

		int numeroVertice = 0;
		int distancia = 0;
	
		for (Vertice v : g.vertices) {
			if (distancia < v.distancia) {
				numeroVertice = v.numero;
				distancia = v.distancia;
			}
		}
		
		Bfs.bfs(g, numeroVertice);

		for (Vertice v : g.vertices) {
			if (distancia < v.distancia)	 
				distancia = v.distancia;
		}
		
		return distancia;
	}

	public static void main(String[] args) 
	{
		// (0) -- (1) -- (2)
		Grafo g1 = new Grafo(3);
	    g1.adicionarAresta(0, 1);
		g1.adicionarAresta(1, 2);

		assert(calcularDiametro(g1) == 2) : "Erro funcao diametro grafo g1"; 

		// (0) -- (1) -- (2)
		//         |
		//        (3) -- (4) -- (5)
		//		  |
		// 		 (6)
		Grafo g2 = new Grafo(7);
	    g2.adicionarAresta(0, 1);
		g2.adicionarAresta(1, 2);
		g2.adicionarAresta(1, 3);
		g2.adicionarAresta(3, 4);
		g2.adicionarAresta(4, 5);
		g2.adicionarAresta(4, 6);

		assert(calcularDiametro(g2) == 4) : "Erro funcao diametro grafo g2"; 

		// (0) -- (1) -- (2)
		//         |
		//        (3) -- (4) -- (5)
		//		  |
		// (6) -- (7) -- (8)
		Grafo g3 = new Grafo(9);
	    g3.adicionarAresta(0, 1);
        g3.adicionarAresta(1, 2);
		g3.adicionarAresta(1, 3);
		g3.adicionarAresta(3, 4);
		g3.adicionarAresta(4, 5);
		g3.adicionarAresta(4, 8);
		g3.adicionarAresta(6, 7);		
		g3.adicionarAresta(7, 8);
		
		assert(calcularDiametro(g3) == 6) : "Erro funcao diametro grafo g3"; 
    	}	
}
