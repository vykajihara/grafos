import java.util.Random;

public class RandomTree {

	private static Random random = new Random();

	public static double diametroMedio(int n)
	{
		int soma = 0;
	
		for (int i = 1; i <= 500; i++) {
			Grafo g = randomWalk(n);

			if (Grafo.eh_arvore(g))
				soma += DiametroGrafo.calcularDiametro(g);
			else
				System.out.println("Erro ao gerar arvore aleatoria");		
		}

		return soma/500.00;
	}

	public static Grafo randomWalk(int n) 
	{
		Grafo g = new Grafo(n);

		for (Vertice u : g.vertices) 
			u.visitado = false;

		int indiceAleatorio = random.nextInt(g.vertices.length);
		g.vertices[indiceAleatorio].visitado = true;

		while (g.qtdArestas < n-1) {
			int outroIndiceAleatorio = random.nextInt(g.vertices.length); 	
		
			if (!g.vertices[outroIndiceAleatorio].visitado) {
				g.adicionarAresta(indiceAleatorio, outroIndiceAleatorio);
				g.vertices[outroIndiceAleatorio].visitado = true;
			}
			indiceAleatorio = outroIndiceAleatorio;
		}

		return g;
	}

	public static void main(String[] args) 
	{	
		for (int i = 250; i <= 2000; i += 250)
			System.out.println(diametroMedio(i));
	}
}

