public class Grafo 
{
	public Vertice[] vertices;
	public int qtdArestas;
	public float[][] pesos;

	public Grafo(int n) 
	{
		this.qtdArestas = 0;

		this.vertices = new Vertice[n];
	    for (int i = 0; i < n; i++)
	    	this.vertices[i] = new Vertice(i);

		pesos = new float[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j<n; j++) {
				pesos[i][j] = Float.MAX_VALUE;
			}
		}
	}

	public void adicionarAresta(int u, int v) 
	{
		this.qtdArestas ++;
		this.vertices[u].listaAdjacentes.add(this.vertices[v]);
		this.vertices[v].listaAdjacentes.add(this.vertices[u]);
	}

	public void adicionarAresta(int u, int v, float peso) 
	{
		this.qtdArestas ++;
		this.vertices[u].listaAdjacentes.add(this.vertices[v]);
		this.vertices[v].listaAdjacentes.add(this.vertices[u]);
		this.pesos[u][v] = peso;
		this.pesos[v][u] = peso;
	}

	public boolean eh_arvore() 
	{
		if (this.qtdArestas != this.vertices.length - 1)
			return false;

		Bfs.bfs(this, 0);
		for (Vertice v : this.vertices) {
			if (v.distancia ==	-1)
				return false;
		}

		return true;
	}

	public void setPesoAresta(int u, int v, float w)
	{
		this.pesos[u][v] = w;
		this.pesos[v][u] = w;
	}

	public static void main(String[] args)
	{
		// (0) -- (1) -- (2)
		//         |
		//        (3) -- (4) -- (5)
		//				  |
		// 				 (6)
		Grafo g1 = new Grafo(7);
	    g1.adicionarAresta(0, 1);
        g1.adicionarAresta(1, 2);
		g1.adicionarAresta(1, 3);
		g1.adicionarAresta(3, 4);
		g1.adicionarAresta(4, 5);
		g1.adicionarAresta(4, 6);

		assert(g1.eh_arvore()) : "Erro funcao eh_arvore grafo1"; 
	
		// (0) -- (1) -- (2)		(5) -- (6) -- (7)
		//         |
		//        (3) -- (4) 
		Grafo g2 = new Grafo(8);
	    g2.adicionarAresta(0, 1);
        g2.adicionarAresta(1, 2);
		g2.adicionarAresta(1, 3);
		g2.adicionarAresta(3, 4);
		g2.adicionarAresta(5, 6);
		g2.adicionarAresta(6, 7);

		assert(!g2.eh_arvore()) : "Erro funcao eh_arvore grafo2";

		// (0) -- (1)
		//  |      |
		// (2) -- (3)
		Grafo g3 = new Grafo(4);
	    g3.adicionarAresta(0, 1);
        g3.adicionarAresta(0, 2);
		g3.adicionarAresta(1, 3);
		g3.adicionarAresta(2, 3);
	
		assert(!g3.eh_arvore()) : "Erro funcao eh_arvore grafo3";
	}
}
