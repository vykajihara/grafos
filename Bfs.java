import java.util.LinkedList;

public class Bfs 
{
	public static void bfs(Grafo g, int s)	
	{
		for (Vertice v : g.vertices) {
			v.distancia = 0;
			v.cor = Vertice.cores.branco;
		}
		
		g.vertices[s].distancia = 0;
		g.vertices[s].cor = Vertice.cores.cinza;

		LinkedList<Vertice> Q = new LinkedList<Vertice>();
		Q.addLast(g.vertices[s]);

		while (Q.size() != 0) {
			Vertice u = Q.remove();
			for (Vertice v : u.listaAdjacentes) {
				if (v.cor == Vertice.cores.branco) {
					v.distancia = u.distancia + 1;
					v.cor = Vertice.cores.cinza;
					Q.addLast(v);
				}
			}
			u.cor = Vertice.cores.preto;	 
		}
	}

	public static void main(String[] args) 
	{
		// (0) -- (1) -- (2)
		Grafo g1 = new Grafo(3);
	    g1.adicionarAresta(0, 1);
        g1.adicionarAresta(1, 2);
		
		bfs(g1, 1);
        assert(g1.vertices[0].distancia == 1) : "Erro BFS 1";
		assert(g1.vertices[1].distancia == 0) : "Erro BFS 2";
		assert(g1.vertices[2].distancia == 1) : "Erro BFS 3";	

		// (0) -- (1) -- (2)
		//         |
		//        (3)
		Grafo g2 = new Grafo(4);
	    g2.adicionarAresta(0, 1);
        g2.adicionarAresta(1, 2);
		g2.adicionarAresta(1, 3);
		
		bfs(g2, 0);
        assert(g2.vertices[0].distancia == 0) : "Erro BFS 4";
		assert(g2.vertices[1].distancia == 1) : "Erro BFS 5";
		assert(g2.vertices[2].distancia == 2) : "Erro BFS 6";	
		assert(g2.vertices[3].distancia == 2) : "Erro BFS 7";

		// (0) -- (1) -- (2)
		//         |
		//        (3) -- (4) -- (5)
		//				  |
		// 				 (6)
		Grafo g3 = new Grafo(7);
	    g3.adicionarAresta(0, 1);
        g3.adicionarAresta(1, 2);
		g3.adicionarAresta(1, 3);
		g3.adicionarAresta(3, 4);
		g3.adicionarAresta(4, 5);
		g3.adicionarAresta(4, 6);
		
		bfs(g3, 3);
        assert(g3.vertices[0].distancia == 2) : "Erro BFS 8";
		assert(g3.vertices[1].distancia == 1) : "Erro BFS 9";
		assert(g3.vertices[2].distancia == 2) : "Erro BFS 10";
		assert(g3.vertices[3].distancia == 0) : "Erro BFS 11";
		assert(g3.vertices[4].distancia == 1) : "Erro BFS 12";
		assert(g3.vertices[5].distancia == 2) : "Erro BFS 13";	
		assert(g3.vertices[6].distancia == 2) : "Erro BFS 14";

		// (0) -- (1) -- (2)		(5) -- (6) -- (7)
		//         |
		//        (3) -- (4) 		
		Grafo g4 = new Grafo(8);
	    g4.adicionarAresta(0, 1);
        g4.adicionarAresta(1, 2);
		g4.adicionarAresta(1, 3);
		g4.adicionarAresta(3, 4);
		g4.adicionarAresta(5, 6);
		g4.adicionarAresta(6, 7);
		bfs(g4, 0);		
		assert(g4.vertices[0].distancia == 0) : "Erro BFS 15";	
		assert(g4.vertices[1].distancia == 1) : "Erro BFS 16";	
		assert(g4.vertices[2].distancia == 2) : "Erro BFS 17";	
		assert(g4.vertices[3].distancia == 2) : "Erro BFS 18";	
		assert(g4.vertices[4].distancia == 3) : "Erro BFS 19";	
		assert(g4.vertices[5].distancia == 0) : "Erro BFS 20";	
		assert(g4.vertices[6].distancia == 0) : "Erro BFS 21";	
		assert(g4.vertices[7].distancia == 0) : "Erro BFS 22";	

		// (0) -- (1)
		//  |      |
		// (2) -- (3)
		Grafo g5 = new Grafo(4);
	    g5.adicionarAresta(0, 1);
        g5.adicionarAresta(0, 2);
		g5.adicionarAresta(1, 3);
		g5.adicionarAresta(2, 3);
		
		bfs(g5, 0);
		assert(g5.vertices[0].distancia == 0) : "Erro BFS 23";	
		assert(g5.vertices[1].distancia == 1) : "Erro BFS 24";	
		assert(g5.vertices[2].distancia == 1) : "Erro BFS 25";	
		assert(g5.vertices[3].distancia == 2) : "Erro BFS 26";	
	}
}

