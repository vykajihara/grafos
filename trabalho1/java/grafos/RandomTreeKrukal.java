package grafos;

import java.util.Arrays;
import java.util.Random;

public class RandomTreeKrukal {
		
    static final int MAX = 2000; //Numero maximo de vertices

    static int parent[]; //Parent e o vertor que contem os conjuntos disjuntos

    //Inicializar o parent com o valor -1
    static void iniParent(int n){
        parent = new int[n];
        for (int i = 0; i < n; i++){
            parent[i] = -1;
        }
    }
    
    static Grafo randomGrafo(Grafo G) {
    
    		Random r = new Random();
			    		
    		float min = 0F; //
    		
    		float max = 1F;
    		
    		//Inicializamos as arestas do grafo completo com pesos aleatorios entre 0 e 1
    		for (int i = 0; i < G.vertices.length-1; i++) {
    			for (int j = i + 1; j < G.vertices.length; j++) {
    				G.arestas[G.qtdArestas].origem = i;
    				G.arestas[G.qtdArestas].destino= j;
    				G.arestas[G.qtdArestas].peso = min + r.nextFloat() * (max - min);
    				G.qtdArestas++;
    			}
    		}
    		return G;
    		
    }
    

    //Find e uma funcao pra achar a raiz do vertice atual
	static int find(int v){
		if(parent[v]==-1){
			return v;
		}
		return find(parent[v]);
	}
	
	//funcao para unir os componentes
	static void union(int v, int u){
		int v_aux = find(v);
		int u_aux = find(u);
		parent[v_aux] = u_aux;
    }
    
    //Verificar se forma um ciclo
    static boolean ciclo(int v, int u){
        if (find(v) == find (u)) {
            return true;
        }
        return false;
    }
    
    
    
    //Funcao Kruskal, primeiro parametro um grafo, e o segundo se vai ser test
    static Aresta[] kruskal(Grafo G) {
    	
    	Aresta resultado[] = new Aresta[G.vertices.length - 1];
    		
    	int origem, destino;
		float peso;
    	float total = 0;		//Total da soma dos pesos da arvore resultante
    	int nArestas = 0;	
    	
    	//Inicializar o parent
    	iniParent(G.vertices.length);
    	
    	//Ordenar as arestas do grafo
    	Arrays.sort(G.arestas, new Aresta());    	

    	//percorrer as arestas ja ordenadas
    	for (int i = 0; i <= G.arestas.length - 1; i++) { 
    		origem = G.arestas[i].origem;
    		destino = G.arestas[i].destino;
    		peso = G.arestas[i].peso;
    		
    		if(!ciclo(origem, destino)) { 				//controlar ciclo
    			total += peso;							//soma os pesos
    			resultado[nArestas] = G.arestas[i];		//Adicionar as arestas para o array resultado
    			union(origem, destino);					//Unir componente
    			nArestas++;
    		}
    	}
    
	    return resultado;
    }
    
    static Grafo RandomKruskalTree(int n) {
    	Grafo G = new Grafo(n);
    	randomGrafo(G);
    	Aresta arestas[] = kruskal(G);
    	G = new Grafo(n);
    	for(int i = 0; i < n - 1; i++) {
    		G.adicionarAresta(arestas[i].origem, arestas[i].destino);
    	}
    	G.arestas = arestas;
    	return G;
    }
    
    public static double diametroMedio(int n)
	{
		int soma = 0;
	
		for (int i = 1; i <= 500; i++) {
			Grafo g = RandomKruskalTree(n);

			if (Grafo.eh_arvore(g))
				soma += DiametroGrafo.calcularDiametro(g);
			else
				System.out.println("Erro ao gerar arvore aleatoria");		
		}

		return soma/500.00;
	}
    
    

    
	public static void main(String[] args) {
		
		{	
			for (int i = 250; i <= 2000; i += 250)
				System.out.println(diametroMedio(i));
		}
		
		
		
		
		//TESTES
		
//		Grafo G = new Grafo(5);
		
		//Para obter uma arvore aleatoria descomentar a funcao randomGrafo
//		randomGrafo(G);
						
//		int origens[] = {0,0,0,0,1,1,1,2,2,3};
//		int destinos[] = {1,2,3,4,2,3,4,3,4,4};
//		float pesos[] = {0.7F, 0.3F, 0.35F, 0.8F, 0.15F, 0.68F, 0.2F, 0.1F, 0.5F, 0.65F};
//		
//		for (int i = 0; i < 10; i++) {
//			G.arestas[i].origem = origens[i];
//			G.arestas[i].destino = destinos[i];
//			G.arestas[i].peso = pesos[i];
//			G.qtdArestas++;
//			
//		}	
//		
//		assert(kruskal(G)[0].origem == 2) : "Erro na origem";		
//		assert(kruskal(G)[0].destino == 3) : "Erro no destino";		
//		assert(kruskal(G)[0].peso == 0.1F) : "Erro no peso";	
//		
//		assert(kruskal(G)[1].origem == 1) : "Erro na origem";		
//		assert(kruskal(G)[1].destino == 2) : "Erro no destino";		
//		assert(kruskal(G)[1].peso == 0.15F) : "Erro no peso";
//		
//		assert(kruskal(G)[2].origem == 1) : "Erro na origem";		
//		assert(kruskal(G)[2].destino == 4) : "Erro no destino";		
//		assert(kruskal(G)[2].peso == 0.2F) : "Erro no peso";
//		
//		assert(kruskal(G)[3].origem == 0) : "Erro na origem";		
//		assert(kruskal(G)[3].destino == 2) : "Erro no destino";		
//		assert(kruskal(G)[3].peso == 0.3F) : "Erro no peso";

		
		
	}
}





































