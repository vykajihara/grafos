package grafos;

public class Grafo 
{
	public Vertice[] vertices;
	public int qtdArestas;
	public Aresta[] arestas;

	public Grafo(int n) 
	{
		this.qtdArestas = 0;
		this.vertices = new Vertice[n];
	    for (int i = 0; i < n; i++)
	    	this.vertices[i] = new Vertice(i);
	    int nArestas = (n * (n-1))/2;
	    arestas = new Aresta[nArestas];
	    for (int i = 0; i < nArestas; i++){
	    	this.arestas[i] = new Aresta();
	    }
	}

	public void adicionarAresta(int u, int v) 
	{
		this.qtdArestas ++;
		this.vertices[u].listaAdjacentes.add(this.vertices[v]);
		this.vertices[v].listaAdjacentes.add(this.vertices[u]);
	}
	
    void printArestas() {
    	System.out.printf("Qdade de arestas: %d\n", this.qtdArestas);
    	for (int i = 0; i < this.qtdArestas; i++) {
    		System.out.printf("%d ---> %d  : %f%n\n", arestas[i].origem, arestas[i].destino, arestas[i].peso);
    	}
    }


	public static boolean eh_arvore(Grafo g) 
	{
		if (g.qtdArestas != g.vertices.length - 1)
			return false;

		Bfs.bfs(g, 0);
		for (Vertice v : g.vertices) {
			if (v.distancia ==	-1)
				return false;
		}

		return true;
	}

	public static void main(String[] args)
	{
		// (0) -- (1) -- (2)
		//         |
		//        (3) -- (4) -- (5)
		//				  |
		// 				 (6)
		Grafo g1 = new Grafo(7);
	    g1.adicionarAresta(0, 1);
        g1.adicionarAresta(1, 2);
		g1.adicionarAresta(1, 3);
		g1.adicionarAresta(3, 4);
		g1.adicionarAresta(4, 5);
		g1.adicionarAresta(4, 6);

		assert(eh_arvore(g1)) : "Erro funcao eh_arvore grafo1"; 
	
		// (0) -- (1) -- (2)		(5) -- (6) -- (7)
		//         |
		//        (3) -- (4) 
		Grafo g2 = new Grafo(8);
	    g2.adicionarAresta(0, 1);
        g2.adicionarAresta(1, 2);
		g2.adicionarAresta(1, 3);
		g2.adicionarAresta(3, 4);
		g2.adicionarAresta(5, 6);
		g2.adicionarAresta(6, 7);

		assert(!eh_arvore(g2)) : "Erro funcao eh_arvore grafo2";

		// (0) -- (1)
		//  |      |
		// (2) -- (3)
		Grafo g3 = new Grafo(4);
	    g3.adicionarAresta(0, 1);
        g3.adicionarAresta(0, 2);
		g3.adicionarAresta(1, 3);
		g3.adicionarAresta(2, 3);
	
		assert(!eh_arvore(g3)) : "Erro funcao eh_arvore grafo3";
	}
}
