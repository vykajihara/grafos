package grafos;

import java.util.LinkedList;

public class Vertice 
{
	enum cores {
		branco,
		cinza,
		preto	
	}	

	public int numero;
	public LinkedList<Vertice> listaAdjacentes;
	public int distancia; 
	public cores cor;
	public boolean visitado;

    public Vertice(int n) 
	{
		this.numero = n;
		this.listaAdjacentes = new LinkedList<Vertice>();
		this.distancia = -1;
		this.cor = cores.branco;
    }

	public void addVerticeListaAdjacentes(Vertice v)
	{
		this.listaAdjacentes.add(v);
	}
}

