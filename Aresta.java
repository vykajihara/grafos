import java.util.Comparator;

class Aresta implements Comparator<Aresta> {
    public int origem, destino;
    public float peso;
    
	@Override
	public int compare(Aresta a1, Aresta a2) {
		if (a1.peso < a2.peso) {
			  return -1;
			} else if (a1.peso > a2.peso) {
			  return 1;
			} else {
			  return 0;
			}	
		}

	@Override
	public String toString() {
		return "Aresta [origem =" + origem + ", destino =" + destino + ", peso =" + peso + "]";
	}
}
